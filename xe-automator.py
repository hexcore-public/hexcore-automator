#!/usr/bin/env python

import sys

import netdatasetup

try:
    import os
    import subprocess
    import distro
    import requests
    from pyxs import Client
    from ipaddress import IPv4Network
    import yaml
    from configobj import ConfigObj
    import debinterface
    import shlex
    import re
except ImportError as e:
    print("Import failed, please run xe-get-update.py")
    print(e)
    print(ImportError)
    sys.exit(1)

DEBUG = 1

IPv4 = None
hostname = None
domain = None
emailAddress = None
password = None


class MyConfigObj(ConfigObj):

    def __init__(self, *args, **kwargs):
        ConfigObj.__init__(self, *args, **kwargs)

    def _write_line(self, indent_string, entry, this_entry, comment):
        if not self.unrepr:
            val = self._decode_element(self._quote(this_entry))
        else:
            val = repr(this_entry)

        return '%s%s%s%s%s' % (indent_string,
                               self._decode_element(self._quote(entry, multiline=False)),
                               self._a_to_u('='),
                               val,
                               self._decode_element(comment))


class MyConfigObjDns(ConfigObj):

    def __init__(self, *args, **kwargs):
        ConfigObj.__init__(self, *args, **kwargs)

    def _write_line(self, indent_string, entry, this_entry, comment):
        if not self.unrepr:
            val = self._decode_element(self._quote(this_entry))
        else:
            val = repr(this_entry)

        return '%s%s%s%s%s' % (indent_string,
                               self._decode_element(self._quote(entry, multiline=False)),
                               self._a_to_u(' '),
                               val,
                               self._decode_element(comment))


def debug(debugstr):
    if DEBUG == 1:
        print(debugstr)
    return


def getos():
    osversion = distro.version(pretty=True)
    debug(osversion)
    return osversion


def getosname():
    osname = distro.name()
    debug(osname)
    return osname


def cleanup(param):
    if osname == 'FreeBSD':
        stream = subprocess.run(["/usr/local/bin/xenstore-rm", param], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("xenstore-rm " + param + "success")
            return
        else:
            debug("xenstore-rm " + param + "error")
            debug(stream.stderr)
            return
    else:
        try:
            param = bytes(param, encoding='utf-8')
            c = Client(xen_bus_path="/dev/xen/xenbus")
            c.connect()
            c.delete(param)
            c.close()
        except:
            print("xenbus is not ready")


def getparam(param):
    if osname == 'FreeBSD':
        stream = subprocess.run(["/usr/local/bin/xenstore-read", param], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            param = stream.stdout.strip()
            return param
        else:
            param = 'not set'
            return param
    else:
        try:
            param = bytes(param, encoding='utf-8')
            c = Client(xen_bus_path="/dev/xen/xenbus")
            c.connect()
            try:
                param = c[param]
            except:
                param = 'not set'
                return param
            c.close()
        except:
            print("xenbus is not ready")
            param = 'not set'
            return param
        return param.decode()


def setnetwork(osversion):
    osclassicdeb = ["16.04 (xenial)", "9 (stretch)", "10 (buster)", "8 (jessie)", "11 (bullseye)", "12 (bookworm)"]
    osnetplandeb = ["24.04 (noble)", "20.04 (focal)", "18.04 (bionic)", "22.04 (jammy)"]
    centbaseold = ["7 (Core)", "7"]
    centbasenew = ["8 (Core)", "33", "8", "34"]
    opensuse = ['15.2']
    oraclelinux = ["8.6", "9.3"]
    nmcli = ["38", "9.2 (Blue Onyx)", "2021.1 (12.4)", "9"]
    netdict = {"ip4": '0', "ip6": '0', "gw4": '0', "nm4": '0', "nm6": '0', "ns4": '0', "ns6": '0'}
    for key, value in netdict.items():
        netdict[key] = getparam("vm-data/" + key)
    debug(netdict)
    if netdict["ip4"] == "not set":
        debug("ip4 param not set")
        return
    global IPv4
    IPv4 = netdict["ip4"]
    netdict["ns4"] = netdict["ns4"].split()
    netdict["ns6"] = netdict["ns6"].split()
    if osversion in nmcli:
        nm4 = IPv4Network("0.0.0.0/" + netdict["nm4"]).prefixlen
        connection_name = "Wired connection 1"
        subprocess.run(["nmcli", "connection", "modify", connection_name, "ipv4.method", "manual"])
        subprocess.run(
                ["nmcli", "connection", "modify", connection_name, "ipv4.addresses", netdict["ip4"] + '/' + str(nm4),
                 "gw4", netdict["gw4"]])
        dns_servers = ",".join(netdict["ns4"])
        subprocess.run(["nmcli", "connection", "modify", connection_name, "ipv4.dns", dns_servers])

        subprocess.run(["nmcli", "connection", "modify", connection_name, "ipv6.method", "auto"])
        subprocess.run(
                ["nmcli", "connection", "modify", connection_name, "ipv6.addresses", netdict["ip6"] + '/' + netdict["nm6"]])

        dns_servers = ",".join(netdict["ns6"])
        subprocess.run(["nmcli", "connection", "modify", connection_name, "ipv6.dns", dns_servers])

        subprocess.run(["nmcli", "networking", "off"])
        subprocess.run(["nmcli", "networking", "on"])

    if osname == 'FreeBSD':
        # set ip
        file = open("/etc/rc.conf.d/network", "w")
        lines1 = "ifconfig_xn0=\"inet " + netdict["ip4"] + " netmask " + netdict["nm4"] + "\"" + "\n"
        file.writelines([lines1])
        file.close()
        # set gw
        file = open("/etc/rc.conf.d/routing", "w")
        lines1 = "defaultrouter=\"" + netdict["gw4"] + "\"" + "\n"
        lines2 = "gateway_enable=\"YES\"" + "\n"
        file.writelines([lines1, lines2])
        file.close()
        stream = subprocess.run(["service", "netif", "restart"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("netif restrt success")
        else:
            debug(stream.stderr)
        stream = subprocess.run(["service", "routing", "restart"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("routing restart success")
        else:
            debug(stream.stderr)
        # set resolve
        file = open("/etc/resolvconf.conf", "w")
        lines1 = 'name_servers="' + ' '.join(netdict["ns4"]) + '"'
        file.writelines([lines1])
        file.close()
        stream = subprocess.run(["resolvconf", "-u"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
        if stream.returncode == 0:
            debug("resolve set success")
        else:
            debug(stream.stderr)
        for key in netdict.keys():
            cleanup("vm-data/" + key)
        return
    if osversion in osclassicdeb:
        debug("set network deb classic")
        confd_path = "/etc/network/interfaces.d"
        inet6_conf_path = confd_path + "/inet6"
        inet_conf_path = "/etc/network/interfaces"
        if os.path.exists(confd_path):
            debug('exist' + " " + confd_path)
        else:
            os.mkdir(confd_path)
        file = open(inet_conf_path, "w")
        file.writelines(["auto lo\n", "iface lo inet loopback\n", "\n", "iface eth0 inet static\n"])
        file.close()
        interfaces = debinterface.Interfaces()
        adapter = interfaces.getAdapter("eth0")
        adapter.setAddressSource("static")
        adapter.setAddrFam("inet")
        adapter.setGateway(netdict["gw4"])
        adapter.setNetmask(netdict["nm4"])
        adapter.setAddress(netdict["ip4"])
        adapter.setDnsNameservers(netdict["ns4"])
        # adapter.setAuto(True)
        adapter.setHotplug(True)
        adapter.setUnknown("source-directory", confd_path)
        debug("Modifying ipv4 interface.")
        interfaces.writeInterfaces()
        if netdict["ip6"] != 'not set' and netdict["ip6"]:
            file = open(inet6_conf_path, "w")
            file.write("iface eth0 inet6 static")
            file.close()
            interfaces6 = debinterface.Interfaces(interfaces_path=inet6_conf_path)
            adapter6 = interfaces6.getAdapter("eth0")
            adapter6.setAddressSource("static")
            adapter6.setAddrFam("inet6")
            adapter6.setAddress(netdict["ip6"])
            adapter6.setNetmask(netdict["nm6"])
            adapter6.setDnsNameservers(netdict["ns6"])
            # adapter6.setAuto(True)
            interfaces6.writeInterfaces()
            debug("Modifying ipv6 interface.")
        os.system("systemctl restart networking")
        os.system("ifdown eth0")
        os.system("ifup eth0")
    if osversion in osnetplandeb:
        netplanconf = "/etc/netplan/01-netcfg.yaml"
        debug("set network netplan")
        nm4 = IPv4Network("0.0.0.0/" + netdict["nm4"]).prefixlen
        if netdict["ip6"] != 'not set' and netdict["ip6"]:
            netconf = {'network': {'version': 2, 'renderer': 'networkd', 'ethernets': {'eth0': {'dhcp4': False,
                                                                                                'dhcp6': False,
                                                                                                'addresses': [netdict[
                                                                                                                  "ip4"] + "/" + str(
                                                                                                    nm4), netdict[
                                                                                                                  "ip6"] + "/" +
                                                                                                              netdict[
                                                                                                                  "nm6"]],
                                                                                                'gateway4': netdict[
                                                                                                    "gw4"],
                                                                                                'nameservers': {
                                                                                                    'addresses':
                                                                                                        netdict["ns4"] +
                                                                                                        netdict[
                                                                                                            "ns6"]}}}}}
        else:
            netconf = {'network': {'version': 2, 'renderer': 'networkd', 'ethernets': {'eth0': {'dhcp4': False,
                                                                                                'dhcp6': False,
                                                                                                'addresses': [netdict[
                                                                                                                  "ip4"] + "/" + str(
                                                                                                    nm4)],
                                                                                                'gateway4': netdict[
                                                                                                    "gw4"],
                                                                                                'nameservers': {
                                                                                                    'addresses':
                                                                                                        netdict[
                                                                                                            "ns4"]}}}}}
        file = open(netplanconf, 'w')
        file.truncate()
        yaml.dump(netconf, file)
        file.close()
        os.system("netplan apply")
    if (osversion in centbaseold) or (osversion in centbasenew) or (osversion in oraclelinux):
        netconfpath = "/etc/sysconfig/network-scripts/ifcfg-eth0"
        if netdict["ip6"] != 'not set' and netdict["ip6"]:
            netconf = {'DEVICE': 'eth0', 'TYPE': 'Ethernet', 'BOOTPROTO': 'none', 'ONBOOT': 'yes',
                       'IPADDR': netdict["ip4"],
                       'NETMASK': netdict["nm4"], 'GATEWAY': netdict["gw4"], 'NM_CONTROLLED': 'yes', 'IPV6INIT': 'yes',
                       'IPV6ADDR': netdict["ip6"] + "/" + netdict["nm6"], 'IPV6_AUTOCONF': 'yes'}
        else:
            netconf = {'DEVICE': 'eth0', 'TYPE': 'Ethernet', 'BOOTPROTO': 'none', 'ONBOOT': 'yes',
                       'IPADDR': netdict["ip4"],
                       'NETMASK': netdict["nm4"], 'GATEWAY': netdict["gw4"], 'NM_CONTROLLED': 'yes', 'IPV6INIT': 'no'}
        config = MyConfigObj(netconf)
        dnsbundle = netdict["ns4"] + netdict["ns6"]
        i = 1
        for dns in dnsbundle:
            config['DNS' + str(i)] = dns
            i += 1
        config.filename = netconfpath
        config.write()
        if osversion in centbaseold:
            os.system("systemctl restart network")
        if osversion in centbasenew:
            if osversion == "38":
                os.system("nmcli conn migrate")
                os.system("nmcli networking off && nmcli networking on")
            else:
                os.system("systemctl restart NetworkManager.service")
                os.system("ifup eth0")
        if osversion in oraclelinux:
            os.system("systemctl restart NetworkManager.service")
            os.system("nmcli networking off && nmcli networking on")
    if osversion in opensuse:
        netconfpath = "/etc/sysconfig/network/ifcfg-eth0"
        routeconfpath = "/etc/sysconfig/network/routes"
        netmenegfconfpath = "/etc/sysconfig/network/config"
        resolvconfpath = "/run/netconfig/resolv.conf"
        nm4 = IPv4Network("0.0.0.0/" + netdict["nm4"]).prefixlen
        if netdict["ip6"] != 'not set' and netdict["ip6"]:
            netconf = {'STARTMODE': 'auto', 'BOOTPROTO': 'static',
                       'IPADDR': netdict["ip4"] + "/" + str(nm4),
                       'IPADDR_0': netdict["ip6"] + "/" + netdict["nm6"]}
        else:
            netconf = {'STARTMODE': 'auto', 'BOOTPROTO': 'static',
                       'IPADDR': netdict["ip4"] + "/" + str(nm4)}
        config = MyConfigObj(netconf)
        config.filename = netconfpath
        config.write()
        file = open(resolvconfpath, 'w')
        file.truncate()
        confignet = MyConfigObj(netmenegfconfpath)
        confignet['NETCONFIG_DNS_POLICY'] = r""
        confignet.write()
        dnsbundle = netdict["ns4"] + netdict["ns6"]
        with open(resolvconfpath, 'w') as resolvconf:
            for server in dnsbundle:
                ns = 'nameserver {0}'.format(server).strip()
                resolvconf.write(ns + "\n")
        file = open(routeconfpath, "w")
        file.write("default " + netdict["gw4"] + " - -")
        file.close()
        os.system("systemctl restart network")
    for key in netdict.keys():
        cleanup("vm-data/" + key)
    return


def sethostname():
    global hostname
    global domain
    hostname = getparam("vm-data/name")
    domain = getparam("vm-data/dm")
    if hostname == "not set":
        debug("name param not set")
        return
    if osname == 'FreeBSD':
        file = open("/etc/rc.conf.d/hostname", "w")
        lines1 = "hostname=\"" + hostname + "\"" + "\n"
        file.writelines([lines1])
        file.close()
        stream = subprocess.run(["/etc/rc.d/hostname", "restart"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("hostname set success")
        else:
            debug(stream.stderr)
    else:
        os.system('%s %s' % ('hostnamectl set-hostname', hostname))
        if domain == "not set":
            debug("domain param not set, write only hostname")
            debug(hostname)
            file = open("/etc/hosts", "w")
            lines1 = "127.0.0.1   localhost" + "\n"
            lines2 = "127.0.0.1   " + hostname + "\n"
            file.writelines([lines1, lines2])
            file.close()
        else:
            debug(hostname)
            debug(domain)
            file = open("/etc/hosts", "w")
            lines1 = "127.0.0.1   localhost" + "\n"
            lines2 = "127.0.0.1   " + hostname + "." + domain + "    " + hostname + "\n"
            file.writelines([lines1, lines2])
            file.close()
    cleanup("vm-data/dm")
    cleanup("vm-data/name")
    return


def setrootpass():
    global password
    password = getparam("vm-data/root_pass")
    if password == "not set":
        debug("password param not set")
        return
    login = "root"
    if osname == 'FreeBSD':
        e = 'echo "' + password + '" | pw usermod -n root -h 0'
        subprocess.call(e, shell=True)
    else:
        try:
            # Using echo and passwd commands to change password
            p = subprocess.Popen(['passwd', login], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            p.stdin.write(f"{password}\n".encode())
            p.stdin.write(f"{password}\n".encode())
            output, err = p.communicate(timeout=15)
            if p.returncode == 0:
                print(f"Password for user {login} successfully changed.")
            else:
                print(f"Error: {err.decode()}")
        except subprocess.TimeoutExpired:
            p.kill()
            print("Operation timeout while changing password.")



        try:
            subprocess.run(['passwd', login], input=password.encode(), check=True)
        except subprocess.CalledProcessError as e:
            print(f"Error: {e}")
    cleanup("vm-data/root_pass")
    return


def setsshkey():
    sshkeyscount = getparam("vm-data/ssh_keys_count")
    if sshkeyscount == "not set":
        debug("ssh keys count param not set")
        return
    sshpath = "/root/.ssh"
    if os.path.exists(sshpath):
        debug('exist' + " " + sshpath)
    else:
        os.mkdir(sshpath)
    authorizedkeys = "/root/.ssh/authorized_keys"
    file = open(authorizedkeys, 'a')
    i = 0
    for i in range(int(sshkeyscount)):
        sshkey = getparam("vm-data/ssh_key" + str(i))
        file.write(sshkey + "\n")
        debug(sshkey)
        i += 1
    file.close()
    cleanup("vm-data/ssh_keys_count")
    for i in range(int(sshkeyscount)):
        cleanup("vm-data/ssh_key" + str(i))
        i += 1
    return


def setresizefs():
    diskresize = getparam("vm-data/disk-resize")
    if diskresize == "not set":
        debug("diskresize  param not set")
        return
    if osname == 'FreeBSD':
        stream = subprocess.run(["gpart", "recover", "ada0"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("gpart recover success")
            stream = subprocess.run(["sysctl", "kern.geom.debugflags=16"], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, encoding='utf-8')
            if stream.returncode == 0:
                debug("sysctl flags=16 success")
                stream = subprocess.run(["gpart", "resize", "-i", "2", "ada0"], stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE, encoding='utf-8')
                if stream.returncode == 0:
                    debug("gpart resize success")
                    stream = subprocess.run(["growfs", "-y", "/dev/ada0p2"], stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE, encoding='utf-8')
                    if stream.returncode == 0:
                        debug("growfs success")
                        stream = subprocess.run(["sysctl", "kern.geom.debugflags=0"], stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE, encoding='utf-8')
                        if stream.returncode == 0:
                            debug("sysctl debugflags=0 success")
                            cleanup("vm-data/disk-resize")
                            return
                        else:
                            debug(stream.stderr)
                            return
                    else:
                        debug(stream.stderr)
                        return
                else:
                    debug(stream.stderr)
                    return
            else:
                debug(stream.stderr)
                return
        else:
            debug(stream.stderr)
            return
    p0 = subprocess.Popen(shlex.split("df /"), stdout=subprocess.PIPE).communicate()
    deviceline = re.search(r'(/[^\s]+)\s', str(p0))
    device = deviceline.group(1)
    partnum = re.findall(r'\d+', device)
    rawdevice = "".join(re.findall(r'\D', device))
    debug(device)
    debug(partnum[0])
    debug(rawdevice)
    p1 = subprocess.Popen(['sgdisk', "-g", "-d", partnum[0], rawdevice],
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = p1.communicate()[0]
    output = output.decode("utf-8")
    debug(output)
    p1 = subprocess.Popen(['sgdisk', "-g", "-N", partnum[0], rawdevice],
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = p1.communicate()[0]
    output = output.decode("utf-8")
    debug(output)
    os.system("partprobe " + rawdevice)
    p3 = subprocess.Popen(['resize2fs', device], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = p3.communicate()[1]
    output = output.decode("utf-8")
    debug(output)
    cleanup("vm-data/disk-resize")
    return


def xeinstallnetdata(osversion):
    netdata = getparam("vm-data/install-netdata")
    if netdata != "1":
        debug("netdata  param not set")
        return
    # IMPORTANT
    ##Чтобы не переделывать установочный скрипт надо создать ссылку на дирректорию с конфигами, т.к. во FreeBSD они в другом месте
    if osname == 'FreeBSD':
        netdataconfpath = "/etc/netdata"
        if os.path.exists(netdataconfpath):
            debug('exist' + " " + netdataconfpath)
        else:
            os.symlink("/usr/local/etc/netdata", "/etc/netdata")
        if os.path.exists("/usr/lib/netdata/conf.d/"):
            debug('exist' + " " + "/usr/lib/netdata")
        else:
            os.makedirs("/usr/lib/netdata")
            os.symlink("/usr/local/lib/netdata/conf.d", "/usr/lib/netdata/conf.d")
    vmhostname = getparam("name")
    apikey = os.path.basename(getparam("vm"))
    netdatasetup.installnetdata(vmhostname, apikey)
    cent = ["8 (Core)", "8"]
    if osversion in cent:
        try:
            os.system("yum update --allowerasing -y")
        except:
            debug("Failed to fix broken packages after install netdata")
    if osname == 'FreeBSD':
        stream = subprocess.run(["sysrc", "netdata_enable=YES"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                encoding='utf-8')
        if stream.returncode == 0:
            debug("netdata enable success")
            stream = subprocess.run(["service", "netdata", "start"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                    encoding='utf-8')
            if stream.returncode == 0:
                debug("netdata start success")
        else:
            debug(stream.stderr)
    cleanup("vm-data/install-netdata")
    return


def xeinstallbbb():
    bbb = getparam("vm-data/install-bbb")
    if bbb != "1":
        debug("bbb  param not set")
        return

    from OpenSSL import crypto, SSL

    # get bbb secret
    bbbSecret = getparam("vm-data/bbb-secret")
    if bbbSecret == "not set":
        debug("Can not get bbb secret from vm-data")
        return

    # make dir for cert
    path = '/opt/ssl'

    if not os.path.exists(path):
        os.makedirs(path)

    if domain == "not set":
        domain = "hexcore-dns.ru"

    # generate selfsigned cert
    global emailAddress
    emailAddress = getparam("vm-data/email")
    emailAddress = getparam("vm-data/email")
    if emailAddress == "not set":
        emailAddress = "admins@hexcore.cloud"
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)
    # create a self-signed cert
    cert = crypto.X509()
    cert.get_subject().C = "XX"
    cert.get_subject().ST = "n/a"
    cert.get_subject().L = "localityName"
    cert.get_subject().O = "organization"
    cert.get_subject().OU = "IT"
    cert.get_subject().CN = hostname + "." + domain
    cert.get_subject().emailAddress = emailAddress
    cert.set_serial_number(0)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, 'sha512')
    with open("/opt/ssl/selfsigned.crt", "wt") as f:
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"))
    with open("/opt/ssl/private.key", "wt") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"))

    # replace cert at nginx config
    with open("/etc/nginx/sites-enabled/bigbluebutton", "r") as file:
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace("/etc/letsencrypt/live/testbbb.hexcore-dns.ru/fullchain.pem", "/opt/ssl/selfsigned.crt")
    filedata = filedata.replace("/etc/letsencrypt/live/testbbb.hexcore-dns.ru/privkey.pem", "/opt/ssl/private.key")
    filedata = filedata.replace("server_name testbbb.hexcore-dns.ru", "server_name " + hostname + "." + domain)

    # Write the file out again
    with open("/etc/nginx/sites-enabled/bigbluebutton", "w") as file:
        file.write(filedata)

    # restart nginx
    os.system("systemctl restart nginx")

    # fix greenlight .env
    with open("/root/greenlight/.env", "r") as file:
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace("BIGBLUEBUTTON_ENDPOINT=https://testbbb.hexcore-dns.ru/bigbluebutton/",
                                "BIGBLUEBUTTON_ENDPOINT=https://" + IPv4 + "/bigbluebutton/")
    filedata = filedata.replace("BIGBLUEBUTTON_SECRET=7YcxenCzNGNUF7rBglbPMLE4tiNMNVCAZj3LN4b4",
                                "BIGBLUEBUTTON_SECRET=" + bbbSecret)
    filedata = filedata.replace("SAFE_HOSTS=testbbb.hexcore-dns.ru",
                                "SAFE_HOSTS=" + hostname + "." + domain + "," + IPv4)

    # Write the file out again
    with open("/root/greenlight/.env", "w") as file:
        file.write(filedata)

    # generate greenlight secret
    os.chdir("/root/greenlight/")
    os.system("docker run --rm bigbluebutton/greenlight:v2 bundle exec rake secret")

    # restart greenlight
    os.system("docker-compose down")
    os.system("docker-compose up -d")

    # generate admin account
    os.system(
        "docker exec greenlight-v2 bundle exec rake user:create['admin','" + emailAddress + "','" + password + "!','admin']")

    # change bbb secret
    try:
        os.system("bbb-conf --setsecret " + bbbSecret)
    except:
        debug("Failed to change bbb secret")

    # change bbb IP
    try:
        os.system("bbb-conf --setip " + IPv4)
    except:
        debug("Failed to change bbb IPv4")

    # change bbb hostname.domain
    if hostname != "not set" and domain != "not set":
        try:
            os.system("bbb-conf --setip " + hostname + "." + domain)
        except:
            debug("Failed to change bbb hostname.domain")
    # restart bbb
    try:
        os.system("bbb-conf --restart")
    except:
        debug("Failed to restart bbb")

    """
    ToDo
    create method to generate vallid cert

    #dry run for certbot to check
    global email
    email = getparam("vm-data/email")
    if email == "not set":
        email = "admins@hexcore.cloud"
    certbotSubprocessTest = subprocess.Popen(["certbot", "certonly", "--dry-run", "-d", hostname + "." + domain, "--nginx", "--email", emailAddress], stdout=subprocess.PIPE)
    certbotResultTest = certbotSubprocessTest.communicate()[0].decode("utf-8")
    if "errors" in certbotResultTest:
        debug("Failed to pass certbot Test verification")
        return
    else:
        debug("Success passed certbot Test verification")

    #replace domain name at nginx config
    with open("/etc/nginx/sites-enabled/bigbluebutton", "r") as file:
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace("testbbb.hexcore-dns.ru", hostname + "." + domain)

    # Write the file out again
    with open("/etc/nginx/sites-enabled/bigbluebutton", "w") as file:
        file.write(filedata)

    #generating new cert with certbot
    certbotSubprocess = subprocess.Popen(["certbot", "certonly", "-d", hostname + "." + domain, "--nginx", "--email", emailAddress], stdout=subprocess.PIPE)
    certbotResult = certbotSubprocess.communicate()[0].decode("utf-8")
    if "errors" in certbotResult:
        debug("Failed to pass certbot verification")
        return
    else:
        debug("Success passed certbot verification")
    """

    cleanup("vm-data/install-bbb")
    cleanup("vm-data/bbb-secret")
    return


def cleanuphistory():
    cleanuphistory = getparam("vm-data/clean_history")
    if cleanuphistory != "1":
        debug("cleanuphistory  param not set")
        return
    if osname == 'FreeBSD':
        historyfile = "/root/.history"
    else:
        historyfile = "/root/.bash_history"
    file = open(historyfile, 'w')
    file.truncate()
    file.close()
    generatesshkey()


def generatesshkey():
    generatesshkey = getparam("vm-data/clean_history")
    if generatesshkey != "1":
        debug("generatesshkey  param not set")
        return
    os.system('%s %s' % ('rm', '/etc/ssh/ssh_host_*'))
    os.system("ssh-keygen -A")
    sshpath = "/root/.ssh"
    if os.path.exists(sshpath):
        debug('exist' + " " + sshpath)
    else:
        os.mkdir(sshpath)
    sshkeypath = "/root/.ssh/id_rsa"
    if os.path.exists(sshkeypath):
        print('exist' + " " + sshkeypath)
    else:
        sshkeygenstr = r'ssh-keygen -b 2048 -t rsa -f ' + sshkeypath + r' -q -N ""'
        os.system(sshkeygenstr)
    cleanup("vm-data/clean_history")
    return


osversion = str(getos())
osname = str(getosname())
vm = getparam("vm")
debug(vm)
cleanuphistory()
setnetwork(osversion)
sethostname()
setrootpass()
setsshkey()
setresizefs()
xeinstallnetdata(osversion)
xeinstallbbb()
