#!/usr/bin/env python
import os
import subprocess
import sys
import shutil
import distro

SAVEPATH = "/usr/sbin/xe-automator"
URL = 'https://bitbucket.org/hexcore-public/hexcore-automator.git'
SAVEPATHFULL = SAVEPATH + "/xe-automator.py"
SAVEPATHREQ = SAVEPATH + "/requirements.txt"
SYSTEMDUNIT = "/etc/systemd/system/xe-automator.service"
PAKG = ['curl', 'gdisk', 'git', 'pip3', 'parted']
osname = distro.name()

def checkutils(utilsname):
    p = shutil.which(utilsname)
    if p is None:
        return False
    return True


def installreq():
    i = 0
    for key in PAKG:
        p = checkutils(PAKG[i])
        if not p:
            print(PAKG[i] + " not found")
            if PAKG[i] == "pip3":
                PAKG[i] = "python3-pip"
            print("Try to install apt " + PAKG[i])
            os.system("apt update")
            p = os.system("apt install -y " + PAKG[i])
            print(p)
            if p != 0:
                print("Try to install yum " + PAKG[i])
                p = os.system("yum install -y " + PAKG[i])
                if p != 0:
                    print("Try to install zypper " + PAKG[i])
                    p = os.system("zypper -n install " + PAKG[i])
                    if p != 0:
                        print("Failed to install dependens")
        i += 1
    if osname == 'FreeBSD':
        p = subprocess.Popen(["/usr/local/bin/pip3", "install", "-r", SAVEPATHREQ], cwd=SAVEPATH, stdout=subprocess.PIPE)
    else:
        try:
            p = subprocess.Popen(["pip3", "install", "-r", SAVEPATHREQ], cwd=SAVEPATH, stdout=subprocess.PIPE)
        except:
            p = subprocess.Popen(["pip3", "install", "-r", SAVEPATHREQ, "--break-system-packages"], cwd=SAVEPATH,
                                 stdout=subprocess.PIPE)
    output = p.communicate()[0]
    p.wait()
    output = output.decode("utf-8")
    print(output)
    if p.returncode != 0:
        print("install requirements failed %d %s" % (p.returncode, output))
        sys.exit(1)
    startxe(final=True)


def checkexistrepo():
    if os.path.exists(SAVEPATH):
        print('exist' + " " + SAVEPATH)
    else:
        os.mkdir(SAVEPATH)
        if osname == 'FreeBSD':
            p = subprocess.Popen(["/usr/local/bin/git", "clone", URL, SAVEPATH], cwd=SAVEPATH, stdout=subprocess.PIPE)
        else:
            p = subprocess.Popen(["git", "clone", URL, SAVEPATH], cwd=SAVEPATH, stdout=subprocess.PIPE)
        output = p.communicate()[0]
        p.wait()
        output = output.decode("utf-8")
        print(output)
        if p.returncode != 0:
            print("git clone failed %d %s" % (p.returncode, output))
            sys.exit(1)
    if os.path.exists(SYSTEMDUNIT):
        print('exist' + " " + SYSTEMDUNIT)
    else:
        try:
            shutil.copy2(SAVEPATH + "/xe-automator.service", SYSTEMDUNIT)
            os.system("systemctl daemon-reload")
            os.system("systemctl enable xe-automator.service")
        except:
            print("Install systemd unit failed")
            return

def startxe(final):
    if osname == 'FreeBSD':
        p = subprocess.Popen(["/usr/local/bin/python", SAVEPATHFULL], stdout=subprocess.PIPE)
    else:
        p = subprocess.Popen(["python3", SAVEPATHFULL], stdout=subprocess.PIPE)
    output = p.communicate()[0]
    p.wait()
    output = output.decode("utf-8")
    print(output)
    if p.returncode != 0:
        if final:
            sys.exit(1)
        else:
            print("failed to start xe-automator.py %d %s" % (p.returncode, output))
            print("Try install requrements")
            installreq()


def updaterepo():
    print("try update exist repo")
    if osname == 'FreeBSD':
        p = subprocess.Popen(["/usr/local/bin/git", "pull"], cwd=SAVEPATH, stdout=subprocess.PIPE)
    else:
        p = subprocess.Popen(["git", "pull"], cwd=SAVEPATH, stdout=subprocess.PIPE)
    output = p.communicate()[0]
    p.wait()
    output = output.decode("utf-8")
    print(output)
    if p.returncode != 0:
        print("git pull failed %d %s" % (p.returncode, output))
        print("Check exist script")
        if os.path.exists(SAVEPATHFULL):
            print("start old xe-automator")
            startxe(final=False)
    startxe(final=False)

checkexistrepo()
updaterepo()
