import subprocess


def import_config(hostname, apikey):
    netdata_conf = "/etc/netdata/netdata.conf"
    stream_conf = "/usr/lib/netdata/conf.d/stream.conf"

    with open(stream_conf, "w") as f:
        f.write("""[stream]
    enabled = yes
    destination = ru-spb-nd1.hexcore.ru:19999
    api key = {}
    timeout seconds = 60
    default port = 19999
    send charts matching = *
    buffer size bytes = 1048576
    reconnect delay seconds = 5
    initial clock resync iterations = 60
""".format(apikey))

    with open(netdata_conf, "w") as f:
        f.write("""[global]
    run as user = netdata
    hostname = {}
    history = 3600
    update every = 10
    bind to = 0.0.0.0
    process scheduling policy = idle
    OOM score = 1000
    error log = none
    access log = none
    debug log = none
    memory mode = none

[health]
    enabled = no

[registry]
    enabled = no

[web]
    web files owner = root
    web files group = netdata
    mode = none

[plugins]
    proc = yes
    diskspace = no
    cgroups = no
    tc = no
    idlejitter = no
    enable running new plugins = no
    check for new plugins every = 60
    slabinfo = no
    fping = no
    ioping = no
    node.d = no
    python.d = no
    go.d = no
    apps = no
    perf = no
    charts.d = no
    netdata = no

[system.net]
    enabled = yes

[system.cpu]
    enabled = yes

[system.uptime]
    enabled = no

[plugin:proc]
    netdata server resources = no
    /proc/mdstat = no
    /proc/net/sockstat = no
    /proc/net/sockstat6 = no
    /proc/net/snmp = no
    /proc/net/snmp6 = no
    /proc/net/netstat = no
    /proc/net/stat/nf_conntrack = no
    /proc/vmstat = no
    /proc/softirqs = no
    /proc/net/softnet_stat = no
    /proc/sys/kernel/random/entropy_avail = no
    /proc/interrupts = no
    /proc/loadavg = no
    ipc = no
    /proc/net/softnet_stat = no

[plugin:proc:/proc/meminfo]
    system swap = no
    system ram = yes
    committed memory = yes
    writeback memory = no
    kernel memory = no
    slab memory = no

[plugin:proc:/proc/diskstats]
    enable new disks detected at runtime = yes
    performance metrics for physical disks = yes
    performance metrics for virtual disks = no
    performance metrics for partitions = no
    bandwidth for all disks = yes
    operations for all disks = no
    merged operations for all disks = no
    i/o time for all disks = no
    queued operations for all disks = no
    utilization percentage for all disks = yes
    backlog for all disks = no
    bcache for all disks = no
    remove charts of removed disks = yes
    exclude disks = loop* ram* sr*
    preferred disk ids = xvd*

[plugin:proc:/proc/net/dev]
    enable new interfaces detected at runtime = auto
    bandwidth for all interfaces = auto
    packets for all interfaces = auto
    errors for all interfaces = no
    drops for all interfaces = no
    fifo for all interfaces = no
    compressed packets for all interfaces = no
    frames, collisions, carrier counters for all interfaces = no

[plugin:proc:/proc/interrupts]
    interrupts per core = no

[plugin:proc:/proc/softirqs]
    interrupts per core = no

[plugin:proc:/proc/net/softnet_stat]
    softnet_stat per core = no

[plugin:proc:/proc/stat]
    per cpu core utilization = yes
    cpu idle states = no
    cpu interrupts = no
    context switches = no
    processes started = no
    processes running = no

[netdata.web_thread1_cpu]
    enabled = no

[netdata.web_thread2_cpu]
    enabled = no

[statsd]
    enabled = no
""".format(hostname))


def installnetdata(hostname, apikey):
    try:
        subprocess.run(
            ["bash", "-c", "bash <(curl -Ss https://get.netdata.cloud/kickstart.sh) --no-updates --non-interactive"],
            check=True)
        import_config(hostname, apikey)
        subprocess.run(["systemctl", "restart", "netdata.service"], check=True)
    except subprocess.CalledProcessError as e:
        print("Error occurred:", e)


